system, "mkdir output";

/**********************************************************************************
*
* MAD-X input script for the extraction optics of the TOF cycle.
* 16/03/2021 - Alexander Huschauer
************************************************************************************/
 
/******************************************************************
 * Energy and particle type definition
 ******************************************************************/

call, file="ps_ext_tof.beam";
BRHO      := BEAM->PC * 3.3356;

/******************************************************************
 * Call lattice files
 ******************************************************************/

call, file="ps.seq";
call, file="ps_ext_tof.str";
call, file="macros.madx";

exec, unassign_QKE16;
exec, unassign_BSW16;

/**********************************************************************************
 *                        Matching using the PFW
 *
 * Values based on non-linear chromaticity measurement along the cycle 
 * recorded on 01.11.2018
 * tunes updated with measurements from 12. April 2023, 12:00 on TOF_23_Test
***********************************************************************************/

! Qx = 0.13094 + 1.22881*x + 19.68008*x^2
Qx   := 0.1488;
Qxp  := 1.22881;
Qxp2 := 19.68008;

! Qy = 0.40405 + 0.86712*x + -24.30586*x^2
Qy   := 0.3145;
Qyp  := 0.86712;
Qyp2 := -24.30586;

use, sequence=PS;
match, use_macro;
        vary, name = k1prpfwf;
        vary, name = k1prpfwd;
        vary, name = k2prpfwf;
        vary, name = k2prpfwd;
        use_macro, name = ptc_twiss_macro(2,0,0);
        constraint, expr = table(ptc_twiss_summary,Q1)  = Qx;
        constraint, expr = table(ptc_twiss_summary,Q2)  = Qy;
        constraint, expr = table(ptc_twiss_summary,DQ1) = Qxp;
        constraint, expr = table(ptc_twiss_summary,DQ2) = Qyp;
jacobian,calls=50000,bisec=3;
ENDMATCH;

use, sequence=PS;
match, use_macro;
        vary, name = k3prpfwf;
        vary, name = k3prpfwd;
        use_macro, name = ptc_twiss_macro(3,0,0);
        constraint, expr = table(nonlin,value,5)  = Qxp2;
        constraint, expr = table(nonlin,value,10) = Qyp2;
jacobian,calls=50000,bisec=3;
ENDMATCH;

/**********************************************************************************
* Extraction bump matching
* Using LHC-type bump settings as initial conditions, but reducing the amplitude 
* from 0.03 to 0.028  
***********************************************************************************/

use, sequence=PS;
kpeqke16 = qke16;

kpebsw12           =    0.0009181692133733964;
kpebsw14           =    0.002432692355230513;
kpebsw20           =    0.0010495074647824034;
kpebsw22           =    0.0025279245167422484;

x0 = 0.028;
px0 = 0.00186;

exec, match_bsw16_bump(1);  

/**********************************************************************************
* Extraction bump knobs
***********************************************************************************/

exec, bsw16_knob_factors(1);

/**********************************************************************************
* High energy orbit corrector knobs
***********************************************************************************/

exec, dhzoc_knob_factors;
exec, write_str_file("./output/ps_ext_tof.str");

/**********************************************************************************
 * PTC Twiss
***********************************************************************************/

! obtain smooth optics functions by slicing the elements
use, sequence=PS;
exec, ptc_twiss_macro(2,0,1);
exec, write_ptc_twiss("./output/ps_ext_tof.tfs");
